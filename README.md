# T16Fling

An Android keyboard with 16 keys (4x4 grid similar to the AOSP numeric keypad) that works by flinging each key in the direction of the desired character.