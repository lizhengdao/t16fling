package es.ideotec.t16fling

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.widget.PopupWindow
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.children
import kotlin.math.absoluteValue

class Key(context: Context, attrs: AttributeSet) : AppCompatImageView(context, attrs) {

    private val tapChar: String
    private val leftChar: String
    private val upChar: String
    private val rightChar: String
    private val downChar: String
    private val special: String

    private val extraKeysId = resources.getIdentifier("${tag}extra", "layout", context.packageName)
    private val popup = PopupWindow(context)
    private val location: IntArray = intArrayOf(0,0)

    init {
        context.obtainStyledAttributes(attrs, R.styleable.Key).apply {
            try {
                tapChar = getString(R.styleable.Key_tap_char) ?: ""
                leftChar = getString(R.styleable.Key_left_char) ?: ""
                upChar = getString(R.styleable.Key_up_char) ?: ""
                rightChar = getString(R.styleable.Key_right_char) ?: ""
                downChar = getString(R.styleable.Key_down_char) ?: ""
                special = getString(R.styleable.Key_special) ?: ""
            } finally {
                recycle()
            }
        }
        if (extraKeysId > 0) {
            val extraKeys = LayoutInflater.from(context).inflate(extraKeysId, null) as ViewGroup
            popup.contentView = extraKeys
            popup.setBackgroundDrawable(BitmapDrawable(resources, "@android:color/transparent"))
            popup.isOutsideTouchable = true
            for (child in extraKeys.children.filter { it is TextView }) {
                val extraKey = child as TextView
                child.setOnClickListener {
                    ime?.currentInputConnection?.commitText(extraKey.text, 1)
                    popup.dismiss()
                }
            }
        }
    }

    private var ime: T16Fling? = null

    fun setIME(ime: T16Fling) {
        this.ime = ime
    }

    private fun enter() {
        ime?.currentInputConnection?.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER))
        ime?.currentInputConnection?.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_ENTER))
    }

    var delDelay: Long = 200
    val delTimerHandler = Handler(Looper.getMainLooper())
    val delTimerRunnable = object: Runnable {
        override fun run() {
            ime?.currentInputConnection?.sendKeyEvent(KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL))
            ime?.currentInputConnection?.sendKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DEL))
            delTimerHandler.postDelayed(this, delDelay)
        }
    }

    private val myListener =  object : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent): Boolean {
            this@Key.isPressed = true

            if (special == "del") {
                delDelay = 200
                val selectedText = ime?.currentInputConnection?.getSelectedText(0) ?: ""
                if (selectedText.isNotEmpty()) {
                    ime?.currentInputConnection?.commitText("", 1)
                }
                delTimerHandler.post(delTimerRunnable)
            }

            return true
        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            when (special) {
                "shift" -> ime?.toggleShift()
                "del" -> {
                    ime?.setShift()
                    return false
                }
                "enter" -> enter()
                else -> {
                    if (tapChar.isNotEmpty()) {
                        ime?.currentInputConnection?.commitText(tapChar, 1)
                        ime?.setShift()
                    }
                }
            }
            if (special != "shift") ime?.setShift()
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            val diffX = e2.x - e1.x
            val diffY = e2.y - e1.y
            val direction = when {
                diffX.absoluteValue > diffY.absoluteValue && diffX < 0 -> "left"
                diffX.absoluteValue < diffY.absoluteValue && diffY < 0 -> "up"
                diffX.absoluteValue > diffY.absoluteValue && diffX > 0 -> "right"
                diffX.absoluteValue < diffY.absoluteValue && diffY > 0 -> "down"
                else -> ""
            }
            when (special) {
                "shift" -> {
                    when (direction) {
                        "up" -> ime?.setCapsLock(true)
                        "down" -> ime?.setCapsLock(false)
                    }
                }
                "del" -> {
                    ime?.setShift()
                    return false
                }
                "enter" -> enter()
                else -> {
                    var char = when {
                        direction == "left" && leftChar.isNotEmpty() -> leftChar
                        direction == "up" && upChar.isNotEmpty() -> upChar
                        direction == "right" && rightChar.isNotEmpty() -> rightChar
                        direction == "down" && downChar.isNotEmpty() -> downChar
                        else -> ""
                    }
                    if (char.isNotEmpty()) ime?.currentInputConnection?.commitText(char, 1)
                }
            }
            if (special != "shift") ime?.setShift()
            return true
        }

        override fun onLongPress(e: MotionEvent?) {
            if (special == "del") {
                delDelay = 100
            } else {
                this@Key.getLocationInWindow(location)
                popup.showAtLocation(this@Key, Gravity.NO_GRAVITY, location[0], location[1]);
            }
        }
    }

    private val detector: GestureDetector = GestureDetector(context, myListener)

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return detector.onTouchEvent(event).let { result ->
            if (!result) {
                if (event.action == MotionEvent.ACTION_UP) {
                    delTimerHandler.removeCallbacks(delTimerRunnable)
                    this.isPressed = false
                    true
                } else false
            } else true
        }
    }

}
